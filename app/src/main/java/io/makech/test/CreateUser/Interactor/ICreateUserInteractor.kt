package io.makech.test.CreateUser.Interactor

/**
 * Created by Zazu on 06/05/19.
 */
interface ICreateUserInteractor {
    fun createUser(parent: Int, active: Boolean, first_name: String, last_name: String)
}