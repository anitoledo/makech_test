package io.makech.test.CreateUser.View

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import io.makech.test.CreateUser.Presenter.CreateUserPresenter
import io.makech.test.MainList.View.MainListView

import io.makech.test.R
import io.makech.test.R.id.navigation
import io.makech.test.Utils.Prefs
import kotlinx.android.synthetic.main.cardview_user.view.*
import kotlinx.android.synthetic.main.create_user_view.view.*
import android.widget.CompoundButton
import android.widget.Toast
import io.makech.test.Navigation.View.ContainerView


/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [CreateUserView.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [CreateUserView.newInstance] factory method to
 * create an instance of this fragment.
 */
class CreateUserView : Fragment(), ICreateUserView {
    private lateinit var viewItem: View
    var prefs: Prefs? = null
    var createUserPresenter: CreateUserPresenter? = null
    var active: Boolean = true;

    override fun cancel() {
        changeFragment()
    }

    override fun createdUser() {
        Toast.makeText(activity!!, "Usuario creado", Toast.LENGTH_LONG).show()
        changeFragment()
    }

    override fun changeFragment() {
        (activity as ContainerView).goToMainList()
    }

    override fun validate() {
        var user_name = viewItem?.name_edittext!!.text.toString().trim()
        var last_name = viewItem?.last_name_edittext!!.text.toString().trim()
        if(user_name.isEmpty() || user_name.isBlank()){
            viewItem?.name_edittext!!.error = "Nombre obligatorio"
        } else {
            createUserPresenter!!.createUser(prefs!!.user_id, active, user_name, last_name)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        viewItem = inflater!!.inflate(R.layout.create_user_view, container, false)

        prefs = Prefs(activity!!.applicationContext)
        createUserPresenter = CreateUserPresenter(this)

        viewItem?.active_switch!!.setOnCheckedChangeListener { _, isChecked ->
            run {
                active = isChecked
            }
        }

        viewItem?.save!!.setOnClickListener { validate() }
        viewItem?.cancel!!.setOnClickListener { cancel() }

        (activity as ContainerView).setActionBarTitle("CREACIÓN")

        return viewItem
    }


}
