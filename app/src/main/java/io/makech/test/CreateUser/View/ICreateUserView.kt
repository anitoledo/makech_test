package io.makech.test.CreateUser.View

import android.view.View

/**
 * Created by Zazu on 06/05/19.
 */
interface ICreateUserView {
    fun createdUser()
    fun changeFragment()
    fun validate()
    fun cancel()
}