package io.makech.test.CreateUser.Interactor

import io.makech.test.CreateUser.Presenter.CreateUserPresenter
import io.makech.test.Models.User
import java.util.*

/**
 * Created by Zazu on 06/05/19.
 */
class CreateUserInteractor(var createUserPresenter: CreateUserPresenter) : ICreateUserInteractor {
    override fun createUser(parent: Int, active: Boolean, first_name: String, last_name: String) {
        var user: User = User(
                parent.toInt(),
                null,
                null,
                active,
                first_name,
                last_name,
                Date()
        )
        user.save()
        createUserPresenter.userCreated()
    }
}