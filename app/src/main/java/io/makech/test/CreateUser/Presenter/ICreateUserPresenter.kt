package io.makech.test.CreateUser.Presenter

/**
 * Created by Zazu on 06/05/19.
 */
interface ICreateUserPresenter {
    fun createUser(parent: Int, active: Boolean, first_name: String, last_name: String)
    fun userCreated()
}