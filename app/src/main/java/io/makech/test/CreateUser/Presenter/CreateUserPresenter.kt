package io.makech.test.CreateUser.Presenter

import io.makech.test.CreateUser.Interactor.CreateUserInteractor
import io.makech.test.CreateUser.View.CreateUserView

/**
 * Created by Zazu on 06/05/19.
 */
class CreateUserPresenter(var createUserView: CreateUserView) : ICreateUserPresenter {

    var createUserInteractor = CreateUserInteractor(this)

    override fun createUser(parent: Int, active: Boolean, first_name: String, last_name: String) {
        createUserInteractor.createUser(parent, active, first_name, last_name)
    }

    override fun userCreated() {
        createUserView.createdUser()
    }

}