package io.makech.test.MainList.Interactor

/**
 * Created by Zazu on 06/05/19.
 */
interface IMainListInteractor {
    fun getActiveUsers(user_id: Int)
    fun getInactiveUsers(user_id: Int)
}