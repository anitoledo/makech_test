package io.makech.test.MainList.Presenter

import io.makech.test.Models.User

/**
 * Created by Zazu on 06/05/19.
 */
interface IMainListPresenter {
    fun getActiveUsers(user_id: Int)
    fun getInactiveUsers(user_id: Int)
    fun showNoActiveUsers()
    fun showNoInactiveUsers()
    fun showActiveUsers(activeUsers: List<User>)
    fun showInactiveUsers(inactiveUsers: List<User>)
}