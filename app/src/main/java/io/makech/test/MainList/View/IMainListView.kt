package io.makech.test.MainList.View

import io.makech.test.Models.User

/**
 * Created by Zazu on 06/05/19.
 */
interface IMainListView {
    fun showNoActiveUsers()
    fun showNoInactiveUsers()
    fun showActiveUsers(activeUsers: List<User>)
    fun showInactiveUsers(inactiveUsers: List<User>)
    fun onItemClicked(user: User)
}