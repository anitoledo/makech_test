package io.makech.test.MainList.Presenter

import io.makech.test.MainList.Interactor.MainListInteractor
import io.makech.test.MainList.View.MainListView
import io.makech.test.Models.User

/**
 * Created by Zazu on 06/05/19.
 */
class MainListPresenter(var mainListView: MainListView) : IMainListPresenter {

    var mainListInteractor = MainListInteractor(this)

    override fun showActiveUsers(activeUsers: List<User>) {
        mainListView.showActiveUsers(activeUsers)
    }

    override fun showInactiveUsers(inactiveUsers: List<User>) {
        mainListView.showInactiveUsers(inactiveUsers)
    }

    override fun getActiveUsers(user_id: Int) {
        mainListInteractor.getActiveUsers(user_id)
    }

    override fun getInactiveUsers(user_id: Int) {
        mainListInteractor.getInactiveUsers(user_id)
    }

    override fun showNoActiveUsers() {
        mainListView.showNoActiveUsers()
    }

    override fun showNoInactiveUsers() {
        mainListView.showNoInactiveUsers()
    }

}