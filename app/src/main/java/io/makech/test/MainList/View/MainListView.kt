package io.makech.test.MainList.View

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import io.makech.test.Adapters.UserRecyclerViewAdapter
import io.makech.test.EditUser.View.EditUserView
import io.makech.test.MainList.Presenter.MainListPresenter
import io.makech.test.Models.User
import io.makech.test.Navigation.View.ContainerView

import io.makech.test.R
import io.makech.test.Utils.Prefs
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.main_list_view.*
import kotlinx.android.synthetic.main.main_list_view.view.*
import android.support.v7.widget.DividerItemDecoration



class MainListView : Fragment(), IMainListView {
    override fun onItemClicked(user: User) {
        var bundle = Bundle()
        bundle.putLong("id", user.id)
        bundle.putBoolean("active", user.active)
        bundle.putString("first_name", user.first_name)
        bundle.putString("last_name", user.last_name)

        val fragment = EditUserView()
        fragment.setArguments(bundle)

        val transaction = activity!!.supportFragmentManager.beginTransaction()
        transaction.setCustomAnimations(R.animator.enter_anim, R.animator.exit_anim);
        transaction.replace(R.id.container, fragment)
        transaction.commit()

    }

    var activeUsersArray = arrayListOf<User>()
    var inactiveUsersArray = arrayListOf<User>()
    private lateinit var viewItem: View
    var activeUsersRecyclerViewAdapter: UserRecyclerViewAdapter? = null
    var inactiveUsersRecyclerViewAdapter: UserRecyclerViewAdapter? = null
    var prefs: Prefs? = null
    var mainListPresenter: MainListPresenter? = null

    override fun showNoActiveUsers() {
        viewItem?.no_active_users.visibility = View.VISIBLE
    }

    override fun showNoInactiveUsers() {
        viewItem?.no_inactive_users.visibility = View.VISIBLE
    }

    override fun showActiveUsers(activeUsers: List<User>) {
        for (user in activeUsers){
            activeUsersArray.add(user)
            activeUsersRecyclerViewAdapter!!.notifyDataSetChanged()
        }
    }

    override fun showInactiveUsers(inactiveUsers: List<User>) {
        for (user in inactiveUsers){
            inactiveUsersArray.add(user)
            inactiveUsersRecyclerViewAdapter!!.notifyDataSetChanged()
        }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        viewItem = inflater!!.inflate(R.layout.main_list_view, container, false)

        mainListPresenter = MainListPresenter(this)
        prefs = Prefs(activity!!.applicationContext)

        val linearLayoutManager = LinearLayoutManager(context)
        linearLayoutManager.orientation = LinearLayoutManager.VERTICAL

        var nav_view = activity!!.findViewById<NavigationView>(R.id.nav_view)
        nav_view.setCheckedItem(R.id.nav_main_list)


        viewItem?.active_contacts!!.setLayoutManager(linearLayoutManager)
        activeUsersRecyclerViewAdapter = UserRecyclerViewAdapter(activeUsersArray, R.layout.cardview_user, activity!!, this)
        viewItem?.active_contacts!!.setAdapter(activeUsersRecyclerViewAdapter)
        viewItem?.active_contacts!!.addItemDecoration(DividerItemDecoration(viewItem?.active_contacts!!.getContext(), DividerItemDecoration.VERTICAL))

        val linearLayoutManager2 = LinearLayoutManager(context)
        linearLayoutManager2.orientation = LinearLayoutManager.VERTICAL

        viewItem?.inactive_contacts!!.setLayoutManager(linearLayoutManager2)
        inactiveUsersRecyclerViewAdapter = UserRecyclerViewAdapter(inactiveUsersArray, R.layout.cardview_user, activity!!, this)
        viewItem?.inactive_contacts!!.setAdapter(inactiveUsersRecyclerViewAdapter)
        viewItem?.inactive_contacts!!.addItemDecoration(DividerItemDecoration(viewItem?.inactive_contacts!!.getContext(), DividerItemDecoration.VERTICAL))

        mainListPresenter!!.getActiveUsers(prefs!!.user_id)
        mainListPresenter!!.getInactiveUsers(prefs!!.user_id)

        (activity as ContainerView).setActionBarTitle("LISTADO")

        return viewItem
    }
}
