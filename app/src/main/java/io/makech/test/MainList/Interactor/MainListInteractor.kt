package io.makech.test.MainList.Interactor

import com.orm.SugarRecord
import io.makech.test.MainList.Presenter.MainListPresenter
import io.makech.test.Models.User
import io.makech.test.Utils.Prefs

/**
 * Created by Zazu on 06/05/19.
 */
class MainListInteractor(var mainListPresenter: MainListPresenter) : IMainListInteractor {
    override fun getInactiveUsers(user_id: Int) {
        var inactiveUsers = SugarRecord.find(User::class.java, "parent = ? and active = ?", user_id.toString(), "0")
        if(inactiveUsers.isEmpty()){
            mainListPresenter.showNoInactiveUsers()
        } else{
            mainListPresenter.showInactiveUsers(inactiveUsers)
        }
    }

    override fun getActiveUsers(user_id: Int) {
        var activeUsers = SugarRecord.find(User::class.java, "parent = ? and active = ?", user_id.toString(), "1")
        if(activeUsers.isEmpty()){
            mainListPresenter.showNoActiveUsers()
        } else{
            mainListPresenter.showActiveUsers(activeUsers)
        }
    }
}