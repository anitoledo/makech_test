package io.makech.test.Adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.app.Activity
import android.view.View
import io.makech.test.MainList.View.MainListView
import io.makech.test.Models.User
import io.makech.test.R
import kotlinx.android.synthetic.main.cardview_user.view.*
import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by Zazu on 07/05/19.
 */
class UserRecyclerViewAdapter constructor(private var users: List<User>, private val resource: Int, private val activity: Activity, val listener: MainListView) : RecyclerView.Adapter<UserRecyclerViewAdapter.UserViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(resource, parent, false)
        return UserViewHolder(view)
    }

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        val userItem = users[position]
        if(userItem.active){
            holder.icon.setImageResource(R.drawable.ic_active_user)
        } else {
            holder.icon.setImageResource(R.drawable.ic_inactive_user)
        }
        holder.user_name.setText(userItem.first_name+" "+userItem.last_name)
        val month_date = SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH)
        val sdf = SimpleDateFormat("yyyy-MM-dd")
        val day = userItem.created_date!!.date.toString()
        val month = userItem.created_date!!.month + 1
        val year = userItem.created_date!!.year + 1900
        val dateString: String = year.toString()+"-"+month.toString()+"-"+day
        val date = sdf.parse(dateString)

        val month_name = month_date.format(date)

        holder.user_created_date.setText(month_name)
        holder.user_layout.setOnClickListener {
            listener.onItemClicked(userItem)
        }
    }

    override fun getItemCount(): Int {
        return users.size
    }


    inner class UserViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val user_name = itemView.user_name
        val user_created_date = itemView.user_created_date
        val user_layout = itemView.user_layout
        val icon = itemView.icon
    }
}