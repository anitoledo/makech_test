package io.makech.test.Login.Presenter

import android.content.Context
import io.makech.test.Login.Interactor.LoginInteractor
import io.makech.test.Login.View.LoginView

/**
 * Created by Zazu on 06/05/19.
 */
class LoginPresenter(var loginView: LoginView): ILoginPresenter {
    override fun success() {
        loginView.hideProgressBar()
        loginView.login()
    }

    var loginIteractor = LoginInteractor(this)

    override fun login(username: String, password: String, context: Context) {
        loginView.showProgressBar()
        loginIteractor.login(username, password, context)
    }

    override fun setError(message: String) {
        loginView.hideProgressBar()
        loginView.setError(message)
    }

}