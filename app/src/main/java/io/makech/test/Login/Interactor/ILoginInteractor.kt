package io.makech.test.Login.Interactor

import android.content.Context

/**
 * Created by Zazu on 06/05/19.
 */
interface ILoginInteractor {
    fun login(username: String, password: String, context: Context)
}