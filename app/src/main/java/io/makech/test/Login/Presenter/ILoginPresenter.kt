package io.makech.test.Login.Presenter

import android.content.Context

/**
 * Created by Zazu on 06/05/19.
 */
interface ILoginPresenter {
    fun login(username: String, password: String, context: Context)
    fun setError(message: String)
    fun success()
}