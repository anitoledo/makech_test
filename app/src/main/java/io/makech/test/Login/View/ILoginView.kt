package io.makech.test.Login.View

import android.view.View

/**
 * Created by Zazu on 06/05/19.
 */
interface ILoginView {
    fun login()
    fun setError(message: String)
    fun validate(view: View)
    fun showProgressBar()
    fun hideProgressBar()
}