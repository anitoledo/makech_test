package io.makech.test.Login.Interactor

import android.content.Context
import com.orm.SugarRecord
import io.makech.test.Login.Presenter.LoginPresenter
import io.makech.test.Models.User
import io.makech.test.Utils.Prefs

/**
 * Created by Zazu on 06/05/19.
 */
class LoginInteractor(var loginPresenter: LoginPresenter) : ILoginInteractor {

    var prefs: Prefs? = null

    override fun login(username: String, password: String, context: Context) {
        var user = SugarRecord.find(User::class.java, "username = ? and password = ?", username, password)
        if(user.isEmpty()){
            loginPresenter.setError("No se encontró ningún usuario con estas credenciales.")
        } else{
            prefs = Prefs(context)
            prefs!!.logged_in = true
            prefs!!.user_id = user.get(0).id.toInt()
            loginPresenter.success()
        }
    }
}