package io.makech.test.Login.View

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import io.makech.test.Login.Presenter.LoginPresenter
import io.makech.test.Navigation.View.ContainerView
import io.makech.test.R
import kotlinx.android.synthetic.main.login_view.*

class LoginView : AppCompatActivity(), ILoginView {

    lateinit var loginPresenter: LoginPresenter

    override fun setError(message: String) {
        error.text = message
        error.visibility = View.VISIBLE
    }

    override fun validate(view: View) {
        var flag = 0
        var username_text = user!!.text.toString().trim()
        var password_text = password!!.text.toString().trim()
        if(username_text.isEmpty() || username_text.isBlank()){
            user.error = "Nombre de usuario obligatorio"
            flag = 1
        }
        if(password_text.isEmpty() || password_text.isBlank()){
            password.error = "Contraseña obligatoria"
            flag = 1
        }
        if(flag == 0){
            loginPresenter.login(username_text, password_text, this)
        }
    }

    override fun showProgressBar() {
        loader.visibility = View.VISIBLE
    }

    override fun hideProgressBar() {
        loader.visibility = View.GONE
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login_view)

        setSupportActionBar(toolbar)

        loginPresenter = LoginPresenter(this)
        toolbar_title.text = "LOGIN"
    }

    override fun login(){
        val intent = Intent(this, ContainerView::class.java)
        startActivity(intent)
    }
}
