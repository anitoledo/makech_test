package io.makech.test

import android.content.Intent
import com.orm.SugarApp
import com.orm.SugarRecord
import io.makech.test.MainList.View.MainListView
import io.makech.test.Models.User
import io.makech.test.Navigation.View.ContainerView
import io.makech.test.Utils.Prefs
import java.sql.Date
import com.orm.SugarRecord.listAll
import com.orm.SugarRecord.findById





/**
 * Created by Zazu on 07/05/19.
 */
class StartApplication : SugarApp() {
    var prefs: Prefs? = null
    var intent: Intent? = null

    override fun onCreate() {
        super.onCreate()

        prefs = Prefs(this)

        var main_user = SugarRecord.findById(User::class.java, 1)
        if(main_user == null) {

            main_user = User(
            null,
            "admin",
            "1234",
            true,
            "Ana",
            "Toledo",
            java.util.Date()
            )
            main_user.save()
        }

        if (prefs!!.logged_in){
            intent = Intent(this, ContainerView::class.java)
            startActivity(intent)
        }
    }
}