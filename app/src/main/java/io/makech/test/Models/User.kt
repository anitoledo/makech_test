package io.makech.test.Models

import com.orm.SugarRecord
import java.util.*

/**
 * Created by Zazu on 07/05/19.
 */
class User : SugarRecord<User> {
    var parent: Int? = null
    var username: String? = null
    var password: String? = null
    var active: Boolean = true
    var first_name: String? = null
    var last_name: String? = null
    var created_date: Date? = null

    constructor() {}

    constructor(parent: Int?, username: String?, password: String?, active: Boolean, first_name: String, last_name: String, created_date: Date) {
        this.parent = parent
        this.username = username
        this.password = password
        this.active = active
        this.first_name = first_name
        this.last_name = last_name
        this.created_date = created_date
    }
}