package io.makech.test.Utils

import android.content.Context
import android.content.SharedPreferences

/**
 * Created by Zazu on 07/05/19.
 */
class Prefs (context: Context) {
    val PREFS_FILENAME = "io.makech.test.prefs"
    val prefs: SharedPreferences = context.getSharedPreferences(PREFS_FILENAME, 0);

    var logged_in: Boolean
        get() = prefs.getBoolean("LOGGED_IN", false)
        set(value) = prefs.edit().putBoolean("LOGGED_IN", value).apply()
    var user_id: Int
        get() = prefs.getInt("USER_ID", 0)
        set(value) = prefs.edit().putInt("USER_ID", value).apply()
}