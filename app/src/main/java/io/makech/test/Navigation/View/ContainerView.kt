package io.makech.test.Navigation.View

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import io.makech.test.Login.View.LoginView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import io.makech.test.CreateUser.View.CreateUserView
import io.makech.test.MainList.View.MainListView
import io.makech.test.R
import io.makech.test.Utils.Prefs
import android.R.attr.versionName
import android.content.Context
import android.view.inputmethod.InputMethodManager


class ContainerView : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener{
    var prefs: Prefs? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        prefs = Prefs(this)

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)

        val view = nav_view.getHeaderView(0)
        val nav_back = view.findViewById<ImageView>(R.id.nav_back) as ImageView

        nav_back.setOnClickListener{
            drawer_layout.closeDrawer(GravityCompat.START)
        }

        val versionName = this.getPackageManager().getPackageInfo(this.getPackageName(), 0).versionName
        version.text = versionName

        val fragment = MainListView()
        addFragment(fragment)

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    fun logout(view: View){
        prefs!!.logged_in = false
        prefs!!.user_id = 0
        val intent = Intent(this, LoginView::class.java)
        startActivity(intent)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_main_list -> {
                val fragment = MainListView()
                addFragment(fragment)
                navigation.setSelectedItemId(R.id.nav_main_list)
            }
            R.id.nav_create -> {
                val fragment = CreateUserView()
                addFragment(fragment)
                navigation.setSelectedItemId(R.id.nav_create)
            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.nav_main_list -> {
                val fragment = MainListView()
                addFragment(fragment)
                nav_view.setCheckedItem(R.id.nav_main_list)
                return@OnNavigationItemSelectedListener true
            }
            R.id.nav_create -> {
                val fragment = CreateUserView()
                addFragment(fragment)
                nav_view.setCheckedItem(R.id.nav_create)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    private fun addFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.setCustomAnimations(R.animator.enter_anim, R.animator.exit_anim);
        transaction.replace(R.id.container, fragment)
        transaction.commit()
    }

    fun setActionBarTitle(title: String) {
        toolbar_title.text = title
    }

    fun goToMainList(){
        navigation.setSelectedItemId(R.id.nav_main_list)

        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0)

        val fragment = MainListView()
        val transaction = supportFragmentManager.beginTransaction()
        transaction.setCustomAnimations(R.animator.enter_anim, R.animator.exit_anim);
        transaction.replace(R.id.container, fragment)
        transaction.commit()
    }
}
