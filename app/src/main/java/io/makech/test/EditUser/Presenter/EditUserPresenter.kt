package io.makech.test.EditUser.Presenter

import io.makech.test.EditUser.Interactor.EditUserInteractor
import io.makech.test.EditUser.View.EditUserView

/**
 * Created by Zazu on 06/05/19.
 */
class EditUserPresenter(var editUserView: EditUserView) : IEditUserPresenter {
    var editUserInteractor = EditUserInteractor(this)

    override fun updateUser(user_id: Long, active: Boolean, first_name: String, last_name: String) {
        editUserInteractor.updateUser(user_id, active, first_name, last_name)
    }

    override fun userUpdated() {
        editUserView.updatedUser()
    }
}