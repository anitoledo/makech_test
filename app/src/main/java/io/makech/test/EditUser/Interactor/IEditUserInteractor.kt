package io.makech.test.EditUser.Interactor

/**
 * Created by Zazu on 06/05/19.
 */
interface IEditUserInteractor {
    fun updateUser(user_id: Long, active: Boolean, first_name: String, last_name: String)
}