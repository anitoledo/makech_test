package io.makech.test.EditUser.Presenter

/**
 * Created by Zazu on 06/05/19.
 */
interface IEditUserPresenter {
    fun updateUser(user_id: Long, active: Boolean, first_name: String, last_name: String)
    fun userUpdated()
}