package io.makech.test.EditUser.View

/**
 * Created by Zazu on 06/05/19.
 */
interface IEditUserView {
    fun updatedUser()
    fun changeFragment()
    fun validate()
    fun cancel()
}