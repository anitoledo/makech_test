package io.makech.test.EditUser.View

import android.content.Context
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Toast
import io.makech.test.EditUser.Presenter.EditUserPresenter
import io.makech.test.MainList.View.MainListView
import io.makech.test.Navigation.View.ContainerView

import io.makech.test.R
import kotlinx.android.synthetic.main.edit_user_view.view.*
import android.view.inputmethod.InputMethodManager.RESULT_HIDDEN
import android.content.Context.INPUT_METHOD_SERVICE
import android.view.inputmethod.InputMethodManager


class EditUserView : Fragment(), IEditUserView {
    private lateinit var viewItem: View
    var editUserPresenter: EditUserPresenter? = null
    var active: Boolean = true
    var user_id: Long = 0
    var first_name: String = ""
    var last_name: String = ""

    override fun changeFragment() {
        (activity as ContainerView).goToMainList()
    }

    override fun validate() {
        var user_name = viewItem?.name_edittext_edit!!.text.toString().trim()
        var last_name = viewItem?.last_name_edittext_edit!!.text.toString().trim()
        if(user_name.isEmpty() || user_name.isBlank()){
            viewItem?.name_edittext_edit!!.error = "Nombre obligatorio"
        } else {
            editUserPresenter!!.updateUser(user_id, active, user_name, last_name)
        }
    }

    override fun cancel() {
        changeFragment()
    }

    override fun updatedUser() {
        Toast.makeText(activity!!, "Usuario actualizado", Toast.LENGTH_LONG).show()
        changeFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        viewItem = inflater.inflate(R.layout.edit_user_view, container, false)

        val bundle = this.arguments
        if (bundle != null) {
            user_id = bundle.getLong("id", 0)
            active = bundle.getBoolean("active", true)
            first_name = bundle.getString("first_name", "")
            last_name = bundle.getString("last_name", "")
        }

        editUserPresenter = EditUserPresenter(this)

        viewItem?.name_edittext_edit.setText(first_name)
        viewItem?.last_name_edittext_edit.setText(last_name)
        viewItem?.active_switch_edit.isChecked = active

        viewItem?.active_switch_edit!!.setOnCheckedChangeListener { _, isChecked ->
            run {
                active = isChecked
            }
        }

        viewItem?.save_edit!!.setOnClickListener { validate() }
        viewItem?.cancel_edit!!.setOnClickListener { cancel() }

        (activity as ContainerView).setActionBarTitle("EDICIÓN")

        return viewItem
    }


}
