package io.makech.test.EditUser.Interactor

import com.orm.SugarRecord
import io.makech.test.EditUser.Presenter.EditUserPresenter
import io.makech.test.Models.User
import java.util.*

/**
 * Created by Zazu on 06/05/19.
 */
class EditUserInteractor(var editUserPresenter: EditUserPresenter) : IEditUserInteractor {
    override fun updateUser(user_id: Long, active: Boolean, first_name: String, last_name: String) {
        var user = SugarRecord.findById(User::class.java, user_id)
        user.first_name = first_name
        user.last_name = last_name
        user.active = active
        user.save()
        editUserPresenter.userUpdated()
    }
}